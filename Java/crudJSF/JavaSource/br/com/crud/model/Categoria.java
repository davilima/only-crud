package br.com.crud.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CAT_CATEGORIA")
@SequenceGenerator(name = "seqCATEGORIA", sequenceName = "SEQ_CATEGORIA", allocationSize = 1)
public class Categoria implements Serializable{
	
	private static final long serialVersionUID = -5842294251238940117L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCATEGORIA")
	@Column(name = "CAT_SEQ_CATEGORIA", precision = 10, scale = 0)
	private int id;
	
	@Column(name = "CAT_NOME_CATEGORIA", nullable = false)
	private String nome;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}	
	

}
