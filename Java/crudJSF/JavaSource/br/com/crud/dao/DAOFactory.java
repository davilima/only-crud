package br.com.crud.dao;

import br.com.crud.dao.contracts.PrsCategoria;

public abstract class DAOFactory {
	
	
	    public static final Class HIBERNATE = HibernateDAOFactory.class;  
	  
	    	 
	    public static DAOFactory instance(Class factory) {  
	        try {  
	            return (DAOFactory)factory.newInstance();  
	        } catch (Exception ex) {  
	            throw new RuntimeException("N�o foi poss�vel criar DAOFactory: " + factory);  
	        }  
	    }  
	  
	    
	    public abstract PrsCategoria getCategoriaDAO();
	    
	 

}
