package br.com.crud.dao.contracts;

import java.util.List;

import br.com.crud.model.Categoria;

public interface PrsCategoria extends PrsDAO<Categoria, Integer>{
	
	public List<Categoria> getAll();
	
	

}
