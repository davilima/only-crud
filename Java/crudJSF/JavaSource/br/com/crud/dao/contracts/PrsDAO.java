package br.com.crud.dao.contracts;

import java.io.Serializable;
import java.util.List;

public interface PrsDAO <T, KeyType extends Serializable> {
	
	public T get(KeyType id) throws Exception;

    public void update(T entity) throws Exception;

    public void delete(T entity) throws Exception;

    public void save(T entity) throws Exception;

    public void deleteById(KeyType id) throws Exception;

    public List<T> getAll();

    public int count();

}
