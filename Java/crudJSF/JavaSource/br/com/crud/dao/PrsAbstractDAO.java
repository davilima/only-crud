package br.com.crud.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;

import br.com.crud.dao.contracts.PrsDAO;
import br.com.crud.util.JPAResourceBean;

public abstract class PrsAbstractDAO<T, KeyType extends Serializable>  
implements PrsDAO<T, KeyType> {	
	
	private Class<T> persistentClass;   
  
    @SuppressWarnings("unchecked")
	public PrsAbstractDAO() {  
        this.persistentClass = (Class<T>) ((ParameterizedType) getClass()  
                                .getGenericSuperclass()).getActualTypeArguments()[0];  
     }  
    
    public Class<T> getPersistentClass() {  
        return persistentClass;  
    } 
  
	
	@Override
	public T get(KeyType id) {
		EntityManager em = JPAResourceBean.getEntityManager();
		T object = null;
	    try {	      
	    	em.getTransaction().begin();
	    	object = em.find(getPersistentClass(), id);
	    	em.getTransaction().commit();
	    } catch (Exception e) {
	    	em.getTransaction().rollback();
	    } finally {
	    	em.close();
	    } 
		
		return object;
	}


	@Override
	public void update(T entity) {
		
		EntityManager em = JPAResourceBean.getEntityManager();
		try {	      
	    	em.getTransaction().begin();	    
	    	em.merge(entity);
	    	em.getTransaction().commit();
	    } catch (Exception e) {
	    	em.getTransaction().rollback();
	    } finally {
	    	em.close();
	    } 
	}

	@Override
	public void delete(T entity)  {
		EntityManager em = JPAResourceBean.getEntityManager();
		try {	      
	    	em.getTransaction().begin();	
	    	em.remove(entity);
	    	em.getTransaction().commit();
	    } catch (Exception e) {
	    	em.getTransaction().rollback();
	    } finally {
	    	em.close();
	    }
	}

	@Override
	public void deleteById(KeyType id) 
	{		
		EntityManager em = JPAResourceBean.getEntityManager();
		try {	      
	    	em.getTransaction().begin();
	    	Object object = em.find(getPersistentClass(), id);
	    	em.remove(object);
	    	em.getTransaction().commit();
	    } catch (Exception e) {
	    	em.getTransaction().rollback();
	    } finally {
	    	em.close();
	    } 
	}

	@Override
	public void save(final T entity)  {
		
		EntityManager em = JPAResourceBean.getEntityManager();
	    try {	      
	    	em.getTransaction().begin();
	    	em.persist(entity);
	    	em.getTransaction().commit();
	    } catch (Exception e) {
	    	em.getTransaction().rollback();
	    } finally {
	    	em.close();
	    } 
	    
	}	

	@Override
	public List<T> getAll() {
		EntityManager em = JPAResourceBean.getEntityManager();
		List<T> list = null;
	    try {	      
	    	em.getTransaction().begin();
	    	String jpql = "select o from " + getPersistentClass().getName() + " o";
	        Query query = em.createQuery(jpql);
	        list = query.getResultList();
	    	em.getTransaction().commit();
	    } catch (Exception e) {
	    	em.getTransaction().rollback();
	    } finally {
	    	em.close();
	    } 
		
		return list;
	}

	@Override
	public int count() {		
		return getAll().size();
	}
	

}
