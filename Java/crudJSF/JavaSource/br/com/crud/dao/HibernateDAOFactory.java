package br.com.crud.dao;

import br.com.crud.dao.contracts.PrsCategoria;

public class HibernateDAOFactory extends DAOFactory{

	@Override
	public PrsCategoria getCategoriaDAO() {
		return (PrsCategoria) instantiateDAO(PrsCategoriaBean.class);
	}
	
	
	 private PrsAbstractDAO instantiateDAO(Class daoClass) {  
	        try {  
	        	PrsAbstractDAO dao = (PrsAbstractDAO) daoClass.newInstance(); 	
	            return dao;  
	        } catch (Exception ex) {  
	            throw new RuntimeException("N�o foi poss�vel instanciar o DAO: " + daoClass, ex);  
	        }  
	    }

	
}
