package br.com.crud.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.crud.dao.contracts.PrsCategoria;
import br.com.crud.model.Categoria;
import br.com.crud.util.JPAResourceBean;

public class PrsCategoriaBean extends  PrsAbstractDAO<Categoria, Integer> implements PrsCategoria {
	
	@Override
	public List<Categoria> getAll()
	{
		EntityManager em = JPAResourceBean.getEntityManager();
		List<Categoria> list = null;
	    try {	      
	    	em.getTransaction().begin();
	    	String jpql = "select o from Categoria o ";
	        Query query = em.createQuery(jpql);
	        list = query.getResultList();
	    	em.getTransaction().commit();
	    } catch (Exception e) {
	    	em.getTransaction().rollback();
	    } finally {
	    	em.close();
	    } 
		
		return list;
		
	}

	
}
