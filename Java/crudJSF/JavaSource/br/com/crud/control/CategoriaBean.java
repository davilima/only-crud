package br.com.crud.control;

import java.io.Serializable;
import java.util.List;

import br.com.crud.dao.DAOFactory;
import br.com.crud.dao.contracts.PrsCategoria;
import br.com.crud.model.Categoria;

public class CategoriaBean implements Serializable{
    
	private static final long serialVersionUID = -6976164725077277570L;
	private Categoria categoria = new Categoria();
    private List<Categoria> categorias;
    private PrsCategoria prsCategoria;
    
    public CategoriaBean()
    {
    	DAOFactory factory = DAOFactory.instance(DAOFactory.HIBERNATE);
    	prsCategoria = factory.getCategoriaDAO();
    	categorias = prsCategoria.getAll();
    }
  
    public String nova(){
        categoria = new Categoria();         
        return "inserir";
    }
     
    public String inserir() throws Exception{
    	prsCategoria.save(categoria);
        return "sucesso";
    }
     
    public void selecionar(int codigo) throws Exception{        
        categoria = prsCategoria.get(codigo);
    }
     
    public String alterar() throws Exception{ 
    	prsCategoria.update(categoria);
        return "sucesso";
    }
     
    public String remover() throws Exception{
    	prsCategoria.deleteById(categoria.getId());
        return "sucesso";
    }    
   
     
    public Categoria getCategoria() {
        return categoria;
    }
 
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
 
    public List<Categoria> getCategorias() {
    	categorias = prsCategoria.getAll();
        return categorias;
    }
    
    public List<Categoria> categorias() {
    	return getCategorias();
    }
 
    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }
}
