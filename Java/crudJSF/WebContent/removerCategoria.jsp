<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
					  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
	  xmlns:f="http://java.sun.com/jsf/core"
	  xmlns:h="http://java.sun.com/jsf/html"
      xmlns:a4j="http://richfaces.org/a4j"
      xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:rich="http://richfaces.org/rich">

<ui:composition template="template.jsp">

<ui:define name="headHtml">
	<title>Remover Categoria</title>
</ui:define>

<ui:define name="conteudo">
        <f:view>
            <h:outputText value="Confirma a remoção da categoria: #{categoriaBean.categoria.nome} ?" />
            <h:form> 
                <h:commandButton value="Sim" action="#{categoriaBean.remover}" />
                <h:commandButton value="Não" action="cancelar" immediate="true" />
            </h:form>
        </f:view>
   </ui:define>
</ui:composition>
</html>