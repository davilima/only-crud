<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
					  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
	  xmlns:f="http://java.sun.com/jsf/core"
	  xmlns:h="http://java.sun.com/jsf/html"
      xmlns:a4j="http://richfaces.org/a4j"
      xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:rich="http://richfaces.org/rich">

<ui:composition template="template.jsp">

<ui:define name="headHtml">
	<title>Inserir Categoria</title>
</ui:define>

<ui:define name="conteudo">
        <f:view>
            <h:form>
            <h:panelGrid columns="3">
                <h:outputText value="Nome" />
                <h:inputText value="#{categoriaBean.categoria.nome}" required="true" requiredMessage="Informe o nome" id="nome" />
                <h:message for="nome" style="color: red;" />
                <h:commandButton value="Salvar" action="#{categoriaBean.inserir}" />
                <h:commandButton value="Cancelar" action="cancelar" immediate="true" />
            </h:panelGrid>    
             
            </h:form>
             
        </f:view>
   </ui:define>
</ui:composition>
</html>