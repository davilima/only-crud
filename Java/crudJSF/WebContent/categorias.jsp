<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
					  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
	  xmlns:f="http://java.sun.com/jsf/core"
	  xmlns:h="http://java.sun.com/jsf/html"
      xmlns:a4j="http://richfaces.org/a4j"
      xmlns:t="http://myfaces.apache.org/tomahawk"
      xmlns:rich="http://richfaces.org/rich">

<ui:composition template="template.jsp">

<ui:define name="headHtml">
	<title>Inserir Categoria</title>
</ui:define>

<ui:define name="conteudo">
        <f:view>
            <h:form>
                <h:commandButton value="Nova" action="#{categoriaBean.nova}" />
            </h:form>            
         
           <h:dataTable value="#{categoriaBean.categorias}" var="c"  >
                <h:column>
                    <f:facet name="header"><h:outputText value="Codigo" /></f:facet>
                    <h:outputText value="#{c.id}" />
                </h:column>
                <h:column>
                    <f:facet name="header"><h:outputText value="Nome" /></f:facet>
                    <h:outputText value="#{c.nome}" />
                </h:column>
                <h:column>
                    <f:facet name="header"><h:outputText value="Ações" /></f:facet>
                    <h:form>
                        <h:commandButton value="Alterar" actionListener="#{categoriaBean.selecionar(c.id)}" action="alterar" />
                        <h:commandButton value="Remover" actionListener="#{categoriaBean.selecionar(c.id)}" action="remover" />
                    </h:form>
                </h:column>
            </h:dataTable>
             
        </f:view>
   </ui:define>
</ui:composition>
</html>